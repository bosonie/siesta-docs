:sequential_nav: next

..  _tutorial-basic-analysis-tools:

Analysis tools
==============

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

.. note::
   Some of the tools mentioned in this tutorial are covered in this
   excellent `slide presentation <https://drive.google.com/file/d/1oQs0xGMoqJ02khKfC1x3jPiwkxwUIHNy/view?usp=sharing>`_
   by Andrei Postnikov.

   
Objectives
----------

Provide a central point for introduction to some of the most basic
*analysis* and *visualization* tools for the electronic structure:

* Band structures
  
* Fermi surfaces

  For this, the eig2bxsf tool can be used. There is material in
  Visual/Fermi_Surfaces, but it needs to be re-generated and checked.
  Note that the k-point grid needs to include the Gamma point, which
  is not the standard thing to do. In practice, one can run an extra
  calculation after initial convergence, re-using the DM and
  requesting a different mesh, and only one scf step (scf needed
  because the .EIG file is needed). (Future: extract eigenvalues in
  EIG-compatible form even from non-scf calculations).

  New examples should be generated. One in 'first-crystals'.
  
* Charge density

  Here explore the three (or more, with sisl) options to plot grid
  things:

  * .RHO file with g2c_ng (former grid2cube)
  * Denchar
  * .RHO file with rho2xsf (or rho2vesta)
  * sisl
    
* LDOS

  Same as above
  
* Wavefunctions

  There is pre-packaged material in Visual/WaveFunctions, but it is
  opaque (prepared as xcrysden scripts).

  Our standard way involving denchar: some setup is needed for the
  definition of the box in which to plot. A new option to denchar (or
  an automatic box info generator) can be added to get the data
  plotted in a unit cell (orthorhombic only; for the rest a new box
  has to be defined).
  
  
* Mulliken charges

  To my knowledge, there is no standard way to plot these, but they
  can be looked at.
  
* (p)DOS

  Two ways:

    * Using the standard block in the fdf file and post-processing the
      .PDOS file.
    * Generating 'COOP' output and using the mprop program in PDOS
      mode. (More options, more file space needed (but filters in the
      fdf can be used to output only interesting bands). Re-use docs
      and examples there.
      
* fat-bands

  Using 'fat' program in the COOP suite. Re-use docs and examples
  there. Despite being in the COOP directory, this belongs to the
  'basic' track.
 
  
* COOP/COHP (deferred to advanced tutorial)
* Spin texture (deferred to advanced tutorial)
* Spins on atoms (e.g., with xsf files with coordinates plus vectors)

  Getting the spins on the atoms can be done by post-processing the
  Mulliken charges, but to my knowledge there is no standard tool for
  this. 
  
* ...
  
These topics should somehow be formally covered here, even if they
have appeared in earlier modules.

.. note:: 
   This is half-way between a tutorial and a how-to. Let's try out a few
   things in different levels:

   * Former uses (e.g. first-encounter) can refer to this for more
     details, or an example from first-encounter could be re-used here.

   * This tutorial should offer a link to a sisl-specific writeup.
     
  







