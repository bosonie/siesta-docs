:sequential_nav: next

..  _tutorial-basic-structure-optimization:

Structural optimization using forces and stresses
=================================================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

.. note::
   Background: Talks on the matter in past tutorials: Emilio Tel Aviv
   2014, Marivi BSC 2017.
   
.. note::
   Objectives: Basic concept of Hellmann-Feynman theorem. Relevant
   output sections and output files. Simple relaxation
   examples: molecule, crystal, variable cell. Constraints. Using
   Lua-based optimizers.
   Further examples in advanced tutorial.
   
   

   
  
  
   

   
