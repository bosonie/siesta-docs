:sequential_nav: next

..  _tutorial-basic-basis-sets:

Basis sets
==========

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will look more closely at the basis sets used in
Siesta.

.. note::
   The required background consists of the talks by Emilio Artacho on
   the subject.

Pre-packaged basis sets
-----------------------

This is actually a very useful feature: if you have already generated (by
whatever means) a basis set that complies with the requirements of
Siesta (i.e., each orbital is the product of a strictly localized
radial function and a spherical harmonic), you can tabulate the radial
part and package it into a ``.ion`` file of the kind produced by
Siesta. Then, you just need to use the option::

   user-basis T

to have the program read the basis set, skipping any other internal
generation step. Actually, the ``.ion`` file contains also
pseudopotential information (via Kleinman-Bylander projectors and the
neutral-atom potential) which should be consistent with the
orbitals.

Note that there must be `.ion` files for all the species in the
calculation. If you have them, there is no need for pseudopotential
files either.  (Except for DFT+U calculations, which need to generate
their 'projectors' using the pseudopotential file)

The content of `.ion` files can be visualized with the tools mentioned
in :ref:`this how-to<how-to-orbital-visualization>`.

You can try these ideas in the `with-ion-files` directory.


Overview of basis-set generation options
----------------------------------------

Simple options
..............

Siesta offers many options for the specification of the basis set. 
Two of the key concepts are the cardinality of the set
and range of the orbitals, which can be specified, in most cases,
simply with a couple of lines. For example::

  PAO.basis-size DZP
  PAO.energy-shift 0.01 Ry

(Recall that the energy-shift is a physical concept determining the
increase in energy brought about by the confinement of the orbital, as
explained in the lecture.). There are one-line options like these
covering parameters such as the split-norm fraction, the
soft-confinement potential, etc. 

One can get quite far in the process of basis selection for a given
problem with one-line options like the above.  See directory ``water-simple``
for an example on water to explore.

The PAO.Basis block
...................

When we specify an option such as `DZP` for the cardinality of the
basis set, Siesta uses internal heuristics to decide which orbitals
are actually needed, which ones to treat as 'polarization orbitals',
etc. For this, it uses information about the ground-state of the
element involved (stored in tables in the program code) and about the
core/valence split of the pseudopotential. Most of the time, the
heuristics work very well, but in certain cases one might need fuller
control over the specification of the orbitals. Also, sometimes it is
needed to set different values of certain parameters for different
species in the calculation. All this can be done through the
``PAO.basis`` block.  For example, the block::
 
  %block PAO.Basis       
  O     2
  n=2    0    2  S 0.15
    0.0  0.0
    1.0  1.0
  n=2    1    3  S 0.18  P 1
    0.0   0.0   0.0
    1.0   1.0   1.0
  H     1      
  n=1    0    2  S 0.25  P 1
    0.0   0.0
    1.0   1.0
  %endblock PAO.Basis

says that `O` should have a 'double zeta' 2s shell, a  'triple zeta'
2p shell, and that the latter should be polarized by an extra
shell. The block specifies also the 'split-norm' parameters to be used
on a shell-by-shell basis.
For `H`, there is a double-zeta 1s shell, polarized, and a requested
value for the split norm. Note that the radii of the orbitals are set
to zero. This means that the program will use any other information
(in this case the split-norm values) to determine them.

The ``PAO.basis`` block can contain a very large number of
options. These are explained in the manual.

In what follows we present just a few examples of uses of the
PAO.Basis blocks for cases of interest.

Elements with semicore states
*****************************

Diffuse orbitals
****************


Ghost orbitals
--------------

Siesta can place basis orbitals anywhere, not just in the position of
atoms. This is useful in certain cases. For example, in the treatment
of surfaces it is sometimes necessary to provide increased variational
freedom to the near-vacuum region, beyond the range of a typical
orbital. In this case one can define an extra layer of 'ghost atoms'
just outside the slab, carrying orbitals. See ref. Garcia-Gil.

Another example is the treatment of certain defect systems, such as
vacancies.

For the purposes of illustrating this feature we have, in directory
``h2o-ghost``, the case of a water molecule with a ghost orbital at the
position of the 'lone pair' of the oxygen atom.

Basis optimization
------------------

The Siesta distribution contains the directory `Util/Optimizer` with
code and examples of a framework that can be used in particular to
optimize basis sets. A full treatment is beyond the scope of this
simple tutorial, but the framework can be easily obtained and set up.


  





   

   
