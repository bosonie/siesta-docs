:sequential_nav: next

..  _tutorial-wannier:

Wannier functions
============================================================

In this tutorial, we will learn how to construct Wannier functions from SIESTA using the SIESTA-Wannier90 interface. The knowledge of Maximally localized Wannier function (MLWF) is assumed. The readers can refer to the review paper on MLWF [MLWF]_ , and the resources on the `Wannier90 website <http://www.wannier.org>`_ . 


We will first learn how to prepare the input files for SIESTA and Wannier90 with the example of Wannierization of valence and conduction bands of :math:`SrTiO_3`. Then we'll move to a slightly more complex example, where the disentanglement is needed. Finally, we will try one application of the Wannier function: plotting of the Fermi surface.

.. note::
   Compilation of Siesta-Wannier is not covered in the installation section.

.. toctree::
   :maxdepth: 1

   SrTiO3/index	      
   graphene/index
   fermisurface/index


.. note::
   This is the traditional way of doing things. There is a new
   method, not yet released officially, in which the wannier90 code
   is used directly by Siesta in `library` mode.



.. [MLWF] Nicola Marzari, Arash A. Mostofi, Jonathan R. Yates, Ivo Souza, and David Vanderbilt,
 Maximally localized Wannier functions: Theory and applications. `Rev. Mod. Phys. 84, 1419 (2012) <https://link.aps.org/doi/10.1103/RevModPhys.84.1419>`_ .
